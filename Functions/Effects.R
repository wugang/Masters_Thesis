
# Collection of examples of function to discuss & understand effect variables

# https://stackoverflow.com/questions/22693939/plotting-a-function-having-different-equations-for-different-range-of-x-in-r

# Libraries
library(ggplot2)

# effect functions

# exponential approach
Know <- function(x) {
    x^0.5
}

# exponential approach
Comm <- function(x) {
    x^0.6
}

# sigmoid function ~(0,0) -> ~(1,1)
Rout <- function(x) {
    1 / (1 + exp(-9 * (x-0.45)))
}

# effects plot
effects <- ggplot(data.frame(x=c(0,1)), aes(x)) +
    stat_function(fun=Know, geom="line", aes(colour="Organizational Knowledge")) +
    stat_function(fun=Rout, geom="line", aes(colour="Organizational Routines")) +
    stat_function(fun=Comm, geom="line", aes(colour="Commitment")) +
    scale_colour_manual(values = c("blue", "red", "forestgreen")) +
    xlab("Input") +
    ylab("Output") +
    theme_minimal() +
    theme(text = element_text(size=17),
          legend.title = element_blank())

effects

ggsave("~/Notes/Projects/Thesis/Images/Plot_Effects.png", width = 10, height = 7)
